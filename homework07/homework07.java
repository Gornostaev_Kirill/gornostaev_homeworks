public class homework07 {

    /**
     * Метод getRareNumber(int[]) возвращает число, которе присутствует в последовательности минимальное количество раз.
     * На вход подается последовательность чисел, оканчивающихся на -1.
     *
     * Гарантируется:
     * Все числа в диапазоне от -100 до 100.
     * Числа встречаются не более 2,147,483,647-раз каждое.
     * Сложность алгоритма - O(n).
     */

    public static int getRareNumber (int[] array) {
        int[] checkSumArray = new int[201];
        int rareNumber = 0;
        int minCount = 200;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != -1) {
                int indexInCheckSumArray = array[i] + 100;
                checkSumArray[indexInCheckSumArray]++;
            } else break;
        }

        for (int i = 0; i < checkSumArray.length; i++) {
            if ((checkSumArray[i] < minCount) && (checkSumArray[i] > 0)) {
                rareNumber = i - 100;
                minCount = checkSumArray[i];
            }
        }
        return rareNumber;
    }

    public static void main(String[] args) {

        int[] array = {5, -9, 20, 45, 2, 98, -9, -5, 20, 45, 20, 98, 2, 2, 45, -5, 98, -1};

        System.out.println("Число, которе присутствует в последовательности минимальное количество раз:\n"
                + getRareNumber(array));
    }

}
