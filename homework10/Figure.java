package homework10;
public  class Figure {

    private  float CoordinateX;
    private  float CoordinateY;


    public float getCoordinateX() {
        return this.CoordinateX;
    }

    public float getCoordinateY() {
        return this.CoordinateY;
    }

    public void setCoordinateX(float coordinateX) {
        this.CoordinateX = coordinateX;
    }

    public void setCoordinateY(float coordinateY) {
        this.CoordinateY = coordinateY;
    }

    public Figure(float coordinateX, float coordinateY) {
        this.CoordinateX = coordinateX;
        this.CoordinateY = coordinateY;

    }

    public double getPerimeter() {
        return 0;
    }
}

