package homework10;

public class Ellipse extends Figure {
    private final double radius1;
    private final double radius2;


    public Ellipse(float coordinateX, float coordinateY, double radius1, double radius2) {
        super(coordinateX, coordinateY);
        this.radius2 = radius1;
        this.radius1 = radius2;
    }

    public double getPerimeter() {
        return (4 * (3.14 * radius1 * radius2 + radius1 - radius2) / (radius1 + radius2));
    }


}

