package homework10;

public class Circle extends Ellipse implements Moving {

    public Circle(float coordinateX, float coordinateY, double radius1) {
        super(coordinateX, coordinateY, radius1, radius1);
    }


    @Override
    public void movingFigure(float newCoordinateX, float newCoordinateY) {
        super.setCoordinateX(newCoordinateX);
        super.setCoordinateY(newCoordinateY);

    }
}

