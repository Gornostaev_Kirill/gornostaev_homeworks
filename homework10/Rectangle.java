package homework10;

public class Rectangle extends Figure {
    private final double a;
    private final double b;

    public Rectangle(float coordinateX, float coordinateY, double a, double b) {
        super(coordinateX, coordinateY);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return a * 2 + b * 2;
    }



}

