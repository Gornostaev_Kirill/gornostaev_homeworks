package homework10;

public class Square extends Rectangle implements Moving {

    public Square(float coordinateX, float coordinateY, double a) {
        super(coordinateX, coordinateY, a, a);
    }

    @Override
    public void movingFigure(float newCoordinateX, float newCoordinateY) {
        super.setCoordinateX(newCoordinateX);
        super.setCoordinateY(newCoordinateY);
    }
}
