package homework10;

public class Main {
    public static void main(String[] args) {
        float newnewcoordinateX = 30;
        float newnewCoordinateY = 45;

        Ellipse ell = new Ellipse(43, 34, 15, 20);
        Rectangle rectangle = new Rectangle(43, 34, 15, 20);
        Square square = new Square(43, 34, 15);
        Circle circle = new Circle(43, 34, 15);


        System.out.println("ellipse" + "Perimeter:" + ell.getPerimeter());
        System.out.println("circle" + "Perimeter:" + circle.getPerimeter());
        System.out.println("rectangle" + "Perimeter:" + rectangle.getPerimeter());
        System.out.println("square" + "Perimeter:" + square.getPerimeter());

        Moving[] relocatableFigures = new Moving[5];

        for (int i =0; i < relocatableFigures.length; i++) {
            if(i%2 == 0){
            relocatableFigures[i] = circle;
            relocatableFigures[i].movingFigure(newnewcoordinateX,newnewCoordinateY);
            System.out.println(circle.getCoordinateX() + " " +circle.getCoordinateY() + " " + circle.getPerimeter());
            }
            else {
                relocatableFigures[i] = square;
                relocatableFigures[i].movingFigure(newnewcoordinateX,newnewCoordinateY);
                System.out.println(square.getCoordinateX() + " " +square.getCoordinateY() + " " + square.getPerimeter());
            }

        }

    }

}

