import java.util.Scanner;

class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();

        if (a < 0) {
            a = a * -1;
        }
        int min = a;
        int ost = 0;


        while (a !=- 1) {
            if (a < 0) {
                a = a * -1;
            }
            if (a == 0){
                min=0;
            }
            while (a != 0) {
                ost = a % 10;
                if (ost < min) {
                    min = ost;
                }
                a /= 10;
                }
            a = scanner.nextInt();
        }

        System.out.println("Result: " + min);
    }
}

